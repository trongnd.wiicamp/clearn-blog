import axios from 'axios';

const axiosUser = axios.create({
  baseURL: 'http://localhost:3000',
  headers: {
    'Content-type': 'application/json',
  },
});

// Làm một số việc gì đó trước trong khi gửi request
// Add a request interceptor
axiosUser.interceptors.request.use((config) => config,
  (error) => Promise.reject(error));

// Add a response interceptor
// Any status code that lie within the range of 2xx cause this function to trigger
// Do something with response data
axiosUser.interceptors.response.use((response) => response.data,
  (error) => Promise.reject(error));

export default axiosUser;
