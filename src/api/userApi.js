import axiosUser from './axiosUser';

const userApi = {
  getAll() {
    const url = '/user';
    return axiosUser.get(url);
  },
};

export default userApi;
