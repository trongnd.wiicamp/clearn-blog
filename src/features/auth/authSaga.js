import {
  call,
  // call,
  delay,
  fork,
  put,
  take,
} from '@redux-saga/core/effects';
import { push } from 'connected-react-router';
// import { push } from 'connected-react-router';
// import { push } from 'connected-react-router';
// import { authActions } from './authSlice';
import { authActions } from './authSlice';

function* handleLogin() {
  try {
    yield delay(2000);
    localStorage.setItem('access_token', 'ok');
    yield put(authActions.login());
    // Redirect admin page
    yield put(push('/home'));
  } catch (error) {
    yield put(authActions.loginFailded(error));
  }
}

function* handleLogout() {
  // yield delay(2000);
  // yield take(authActions.logout.type);
  // console.log('Handle logout');
  localStorage.removeItem('access_token');
  yield put(authActions.logout());
  yield put(push('/'));
}

// function* heloSaga(a) {
//   yield delay(1000);
//   const data = put(userApi.getAll().then((response) => response));
//   console.log(a);
//   const test = data.payload.action;
//   console.log(test);
// }

function* watchLoginFlow() {
  // const user = yield take('USER_FETCH_REQUESTED');
  // yield fork(heloSaga, user);
  while (true) {
    const isLoggedIn = Boolean(localStorage.getItem('access_token'));
    if (!isLoggedIn) {
      const action = yield take('LOGIN_REQUEST');
      // console.log('testloginsaga');
      yield fork(handleLogin, action.payload);
    }

    yield take('LOGOUT_REQUEST');
    yield call(handleLogout);

    // Đứng đợi logout
    // yield call(handleLogout);
  }
}

export default function* authSaga() {
  yield fork(watchLoginFlow);
}
