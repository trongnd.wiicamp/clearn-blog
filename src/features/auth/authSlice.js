import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  isLoggedIn: false,
  logging: Boolean(localStorage.getItem('access_token')),
  loading: false,
  curentUser: null,
  user: null,
};

const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    login(state) {
      const currentState = state;
      currentState.logging = Boolean(localStorage.getItem('access_token'));
    },

    loginSuccess(state, action) {
      const currentState = state;
      currentState.logging = false;
      currentState.curentUser = action.payload;
    },

    loginFailded(state) {
      const currentState = state;
      currentState.logging = false;
    },

    logout(state) {
      // console.log('logout start');
      const currentState = state;
      // currentState.isLoggedIn = false;
      currentState.logging = Boolean(localStorage.getItem('access_token'));
      currentState.loading = false;
      currentState.curentUser = null;
    },

    checkLoading(state) {
      const currentState = state;
      currentState.loading = true;
    },

    resetLoading(state) {
      const currentState = state;
      currentState.loading = false;
    },
  },
});
// Actions
export const authActions = authSlice.actions;

// Selector
export const selectIsLoggedIn = (state) => state.auth.isLoggedIn;
export const selectIsLogging = (state) => state.auth.logging;
export const selectIsLoading = (state) => state.auth.loading;

// Reducers
const authReducer = authSlice.reducer;
export default authReducer;
