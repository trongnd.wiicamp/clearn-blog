import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  listPosts: {},
  loading: false,
  pagination: {
    _page: 1,
    _limit: 10,
  },
};

export const studentSlice = createSlice({
  name: 'posts',
  initialState,
  reducers: {
    getUser: (state, action) => {
      const currentState = state;
      currentState.listPosts = action.payload;
    },
    checkLoading: (state) => {
      const currentState = state;
      currentState.loading = true;
    },

    resetLoadingPostItem: (state) => {
      const currentState = state;
      currentState.loading = false;
    },
    changePage: (state, action) => {
      const currentState = state;
      currentState.pagination._page = action.payload;
    },
  },
});

export const {
  resetLoadingPostItem, getUser, checkLoading, changePage,
} = studentSlice.actions;
export const selectPosts = (state) => state.posts.listPosts;
export const selectLoading = (state) => state.posts.loading;
export const selectPagination = (state) => state.posts.pagination;
export default studentSlice.reducer;
