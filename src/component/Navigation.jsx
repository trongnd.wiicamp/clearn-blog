import React from 'react';
import { Link, NavLink } from 'react-router-dom';
import { useAppDispatch, useAppSelector } from '../app/hooks';
import { selectIsLogging } from '../features/auth/authSlice';
// import { authActions } from '../features/auth/authSlice';
// import { authActions } from '../features/auth/authSlice';

const Navigation = () => {
  const dispatch = useAppDispatch();
  const logging = useAppSelector(selectIsLogging);

  return (
    <nav className="navbar navbar-expand-lg navbar-light" id="mainNav">
      <div className="container px-4 px-lg-5">
        <Link className="navbar-brand" to="/">
          Start Bootstrap
        </Link>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarResponsive"
          aria-controls="navbarResponsive"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          Menu
          <i className="fas fa-bars" />
        </button>
        <div className="collapse navbar-collapse" id="navbarResponsive">
          <ul className="navbar-nav ms-auto py-4 py-lg-0">
            <li className="nav-item">
              <NavLink
                exact
                className="nav-link px-lg-3 py-3 py-lg-4"
                to="/"
                activeClassName="active rou-nav-link"
              >
                Login
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                exact
                className="nav-link px-lg-3 py-3 py-lg-4"
                to="/home"
                activeClassName="active rou-nav-link"
              >
                Home
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                className="nav-link px-lg-3 py-3 py-lg-4"
                to="/about"
                activeClassName="active rou-nav-link"
              >
                About
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                className="nav-link px-lg-3 py-3 py-lg-4"
                to="/post"
                activeClassName="active rou-nav-link"
              >
                Sample Post
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                className="nav-link px-lg-3 py-3 py-lg-4"
                to="/contact"
                activeClassName="active rou-nav-link"
              >
                Contact
              </NavLink>
            </li>
            <li className="nav-item">

              {logging && (
                <button
                  onClick={() => dispatch({ type: 'LOGOUT_REQUEST' })}
                  type="button"
                  className="px-lg-3 py-3 py-lg-4 nav-btn"
                >
                  Log out
                </button>
              )}
            </li>

          </ul>
        </div>
      </div>
    </nav>
  );
};

export default Navigation;
