/* eslint-disable react/require-default-props */
import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';
import ReactLoading from 'react-loading';
import { Link } from 'react-router-dom';
import postsApi from '../api/postApi';
import { useAppDispatch, useAppSelector } from '../app/hooks';
import postBg from '../assets/images/post-bg.jpg';
import { checkLoading, selectLoading } from '../features/students/studentSlice';

const PostItem = ({ match }) => {
  const { id } = match.params;
  const [postItem, setPostItem] = useState([]);
  const dispatch = useAppDispatch();
  const loading = useAppSelector(selectLoading);

  useEffect(() => {
    postsApi.getById(id).then((item) => {
      setPostItem(item);
      dispatch(checkLoading());
    });
  }, [dispatch, id, loading]);

  return (
    <>
      <header className="masthead" style={{ backgroundImage: `url(${postBg})` }}>
        <div className="container position-relative px-4 px-lg-5">
          <div className="row gx-4 gx-lg-5 justify-content-center">
            <div className="col-md-10 col-lg-8 col-xl-7">
              <div className="post-heading">
                <h1>Man must explore, and this is exploration at its greatest</h1>
                <h2 className="subheading">Problems look mighty small from 150 miles up</h2>
                <span className="meta">
                  Posted by
                  <a href="#!">Start Bootstrap</a>
                  on August 24, 2021
                </span>
              </div>
            </div>
          </div>
        </div>
      </header>
      {!loading
        ? <ReactLoading className="custom-loading " type="spin" color="black" height={500} width={100} />
        : (
          <article className="mb-4">
            <Link
              to="/home"
              style={{ width: 'fit-content' }}
              className="back__home d-block m-auto btn mt-5 mb-5 bg-success border border-light rounded"
            >
              Back home
            </Link>

            <div className="container px-4 px-lg-5">
              <div className="row gx-4 gx-lg-5 justify-content-center">
                <div className="col-md-10 col-lg-8 col-xl-7 text-center">
                  <h2>
                    {postItem.title}
                  </h2>
                  <div>
                    <img width="100%" height="200" src={postItem.imageUrl} alt="" />
                  </div>
                  <p>
                    {postItem.description}
                  </p>
                  <p>
                    Post by
                    {postItem.author}
                  </p>
                </div>
              </div>
            </div>
          </article>
        )}
    </>
  );
};

PostItem.propTypes = {
  match: PropTypes.objectOf(PropTypes.shape),
};

export default PostItem;
