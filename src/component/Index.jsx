import React from 'react';
import ReactLoading from 'react-loading';
import { Link } from 'react-router-dom';
import { useAppDispatch, useAppSelector } from '../app/hooks';
import backgroundHeader from '../assets/images/home-bg.jpg';
import { resetLoadingPostItem, selectLoading, selectPosts } from '../features/students/studentSlice';
import Pagination from './Pagination';

// eslint-disable-next-line react/prop-types
const Index = () => {
  const posts = useAppSelector(selectPosts);
  const loading = useAppSelector(selectLoading);
  const dispatch = useAppDispatch();
  // const match = useRouteMatch();

  const handleResetLoading = () => dispatch(resetLoadingPostItem());

  return (
    <>
      <header
        className="masthead"
        style={{ backgroundImage: `url(${backgroundHeader})` }}
      >
        <div className="container position-relative px-4 px-lg-5">
          <div className="row gx-4 gx-lg-5 justify-content-center">
            <div className="col-md-10 col-lg-8 col-xl-7">
              <div className="site-heading">
                <h1>Clean Blog</h1>
                <span className="subheading">A Blog Theme by Start Bootstrap</span>
              </div>
            </div>
          </div>
        </div>
      </header>

      {!loading && <ReactLoading className="custom-loading" type="spin" color="black" height={500} width={100} />}
      <div className="container px-4 px-lg-5">
        <div className="row gx-4 gx-lg-5 justify-content-center">
          <div className="col-md-10 col-lg-8 col-xl-7">
            {
              loading
              && (
                <div>
                  {Array.from(posts.data).map((post) => (
                    <div key={post.id} className="text-center line__bottom">
                      <div className="post-preview">
                        <Link onClick={handleResetLoading} to={`post/${post.id}`}>
                          <h2 className="post-title">
                            {post.title}
                          </h2>
                          <h3 className="post-subtitle text-truncate-2">
                            {post.description}
                          </h3>
                        </Link>
                        <p className="post-meta">
                          Posted by
                          <a href="#!">{post.author}</a>
                          on September 24, 2021
                        </p>
                      </div>
                    </div>
                  ))}
                  <Pagination />
                </div>
              )
            }
          </div>
        </div>
      </div>
    </>
  );
};

export default Index;
