import React, { useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import { useAppDispatch, useAppSelector } from '../app/hooks';
import { changePage, selectPosts } from '../features/students/studentSlice';

const Pagination = () => {
  const { _page } = useParams();
  const data = useAppSelector(selectPosts);
  const { pagination } = data;
  const { _limit, _totalRows } = pagination;
  const totalPage = Math.ceil(_totalRows / _limit);
  const dispatch = useAppDispatch();

  const [currentNumber, setCurrentNumber] = useState(parseInt(_page));

  const pageNumer = [];
  for (let i = 1; i <= totalPage; i += 1) {
    pageNumer.push(i);
  }

  const changeNumber = (event) => {
    const { target } = event;
    const num = parseInt(target.innerHTML, 10);

    dispatch(changePage(num));
    setCurrentNumber(num);
  };

  return (
    <div className="d-flex justify-content-center  mt-4 mb-4">
      <ul className="pagination">
        {
          pageNumer.map((number) => (
            <li key={number} className="list-style-none">
              <Link to={`/home/${number}`} className={(currentNumber === number) ? 'active' : ''} onClick={(event) => { changeNumber(event); }}>{number}</Link>
            </li>
          ))
        }
      </ul>
    </div>
  );
};

export default Pagination;
