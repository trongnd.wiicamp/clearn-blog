// import { CircularProgress } from '@material-ui/core';
import { CircularProgress } from '@material-ui/core';
import React, { useState } from 'react';
import { useAppDispatch, useAppSelector } from '../app/hooks';
import backgroundHeader from '../assets/images/home-bg.jpg';
import { authActions, selectIsLoading } from '../features/auth/authSlice';

const LoginForm = () => {
  const dispatch = useAppDispatch();
  // const [user, setUser] = useState({});
  const [checkUser, setCheckUser] = useState(false);
  const [currentUser, setCurrentUser] = useState({
    email: '',
    password: '',
  });
  const isLoggedIn = useAppSelector(selectIsLoading);

  function handleLoginClick() {
    setCheckUser(false);
    // if (user) {
    //   if (user.email === currentUser.email && user.password === currentUser.password) {
    //     dispatch({ type: 'LOGIN_REQUEST' });
    //     dispatch(authActions.checkLoading());
    //     // setCheckUser(false);
    //   } else {
    //     setCheckUser(true);
    //     dispatch(authActions.resetLoading());
    //   }
    // }
    // if (!user) {
    if (currentUser.email === 'trong@gmail.com' && parseInt(currentUser.password) === parseInt(1)) {
      dispatch({ type: 'LOGIN_REQUEST' });
      dispatch(authActions.checkLoading());
      // setCheckUser(false);
    } else {
      setCheckUser(true);
      dispatch(authActions.resetLoading());
    }
    // }
    // dispatch({ type: 'USER_FETCH_REQUESTED', payload: currentUser });
  }

  function onHandleFocus() {
    setCheckUser(false);
  }

  function onhandleChange(e) {
    const { target } = e;
    const { name } = target;
    const { value } = target;

    setCurrentUser({
      ...currentUser,
      [name]: value,
    });
  }

  return (
    <div className="login__box">
      <header
        className="masthead"
        style={{ backgroundImage: `url(${backgroundHeader})` }}
      >
        <div className="container position-relative px-4 px-lg-5">
          <div className="row gx-4 gx-lg-5 justify-content-center">
            <div className="col-md-10 col-lg-8 col-xl-7">
              <div className="site-heading">
                <h1>Login Page</h1>
                <span className="subheading">A Blog Theme by Start Bootstrap</span>
              </div>
            </div>
          </div>
        </div>
      </header>
      <form className="login__form" onSubmit={(e) => e.preventDefault()}>
        <div className="form-group">
          <label className="w-100" htmlFor="exampleInputEmail1">
            Email: trong@gmail.com
            <input
              type="email"
              name="email"
              // value={currentUser.email}
              onFocus={onHandleFocus}
              className="form-control"
              onChange={onhandleChange}
              id="exampleInputEmail1"
              aria-describedby="emailHelp"
              placeholder="Enter email"
            />
          </label>
        </div>
        <div className="form-group">
          <label className="w-100" htmlFor="exampleInputPassword1">
            Password: 1
            <input
              type="password"
              name="password"
              // value={currentUser.password}
              onFocus={onHandleFocus}
              onChange={onhandleChange}
              className="form-control"
              id="exampleInputPassword1"
              placeholder="Password"
            />
          </label>
        </div>
        <button onClick={handleLoginClick} type="submit" className="d-flex justify-content-center align-align-items-center btn btn-primary p-0">
          {isLoggedIn && <CircularProgress size={20} color="inherit" className="mt-2" />}
          &nbsp;
          <span style={{ lineHeight: '35px' }}>Login</span>
        </button>
        {checkUser && <p className="show__err">User name, pass word error !</p>}
      </form>
    </div>
  );
};
export default LoginForm;
