import React from 'react';

const NOTFOUND = () => (
  <div>
    <div className="banner-fake">
      Banner fake login
    </div>
    <h2 className="text-center pt-5 pb-5">NOT FOUND</h2>
  </div>
);

export default NOTFOUND;
