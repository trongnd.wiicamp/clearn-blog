import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { selectIsLogging } from '../features/auth/authSlice';
import { useAppSelector } from '../app/hooks';

// eslint-disable-next-line react/prop-types
const PrivateRoute = ({ component: Component, ...rest }) => {
  const logging = useAppSelector(selectIsLogging);

  return (
    <Route
      {...rest}
      render={(props) => (
        logging ? <Component {...props} /> : <Redirect to="/" />
      )}
    />
  );
};

export default PrivateRoute;
