import React, { useEffect } from 'react';
import { Switch, Route } from 'react-router-dom';
import postsApi from './api/postApi';
import { useAppDispatch, useAppSelector } from './app/hooks';
import About from './component/About';
import Contact from './component/Contact';
import Footer from './component/Footer';
import Index from './component/Index';
import IndexPagi from './component/IndexPagi';
import LoginForm from './component/LoginForm';
import Navigation from './component/Navigation';
import Post from './component/Post';
import PostItem from './component/PostItem';
// import Route from './navigation/Route';
import { checkLoading, getUser, selectPagination } from './features/students/studentSlice';
import './loginform.css';
import NOTFOUND from './navigation/NOTFOUND';
import PrivateRoute from './navigation/PrivateRoute';
import './style.css';

function App() {
  const dispatch = useAppDispatch();
  const pagination = useAppSelector(selectPagination);

  useEffect(() => {
    postsApi.getAll(pagination).then((response) => {
      dispatch(getUser(response));
      dispatch(checkLoading());
    });
  }, [dispatch, pagination]);

  return (
    <>
      <Navigation />
      <Switch>
        <Route exact path="/" component={LoginForm} />
        <PrivateRoute path="/home" exact component={Index} />
        <PrivateRoute path="/home/:_page" component={IndexPagi} />
        <PrivateRoute path="/about" component={About} />
        <PrivateRoute path="/contact" component={Contact} />
        <PrivateRoute path="/post" exact component={Post} />
        <PrivateRoute path="/post/:id" component={PostItem} />
        <PrivateRoute path="*" component={NOTFOUND} />
      </Switch>
      <Footer />
    </>
  );
}

export default App;
