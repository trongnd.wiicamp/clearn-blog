import { combineReducers, configureStore } from '@reduxjs/toolkit';
import { connectRouter, routerMiddleware } from 'connected-react-router';
import createSagaMiddleware from 'redux-saga';
import authReducer from '../features/auth/authSlice';
import studentReducer from '../features/students/studentSlice';
import history from '../utils/history';
import rootSaga from './rootSaga';

const rootReducer = (historyArg) => combineReducers({
  router: connectRouter(historyArg),
  posts: studentReducer,
  auth: authReducer,
});

const sagaMiddleware = createSagaMiddleware();

const store = configureStore({
  reducer: rootReducer(history),
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(
    sagaMiddleware,
    routerMiddleware(history),
  ),
});

sagaMiddleware.run(rootSaga);

export default store;
