import { useDispatch, useSelector } from 'react-redux';

// Setup useDispatch và useSelector dùng cho tất cả app
export const useAppDispatch = () => useDispatch();
export const useAppSelector = useSelector;
