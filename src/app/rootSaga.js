import { all } from 'redux-saga/effects';
import authSaga from '../features/auth/authSaga';
import helloSaga from '../redux/saga/saga';

export default function* rootSaga() {
  yield all([
    helloSaga(),
    authSaga(),
  ]);
}
